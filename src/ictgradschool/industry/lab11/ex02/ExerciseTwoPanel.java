package ictgradschool.industry.lab11.ex02;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * A simple JPanel that allows users to add or subtract numbers.
 *
 * TODO Complete this class. No hints this time :)
 */
public class ExerciseTwoPanel extends JPanel implements ActionListener{

    /**
     * Creates a new ExerciseFivePanel.
     */

    private JButton addButton;
    private JButton subtractButton;
    private JTextField numOne;
    private JTextField numTwo;
    private JTextField result;

    public ExerciseTwoPanel() {
        setBackground(Color.white);
        addButton = new JButton("add");
        subtractButton = new JButton("subtract");
        numOne = new JTextField(10);
        numTwo = new JTextField(10);
        result = new JTextField(20);

        JLabel resultLabel = new JLabel("Result:");

        this.add(numOne);
        this.add(numTwo);
        this.add(addButton);
        this.add(subtractButton);
        this.add(resultLabel);
        this.add(result);

        addButton.addActionListener(this);
        subtractButton.addActionListener(this);
    }

    public void actionPerformed(ActionEvent event){
        double numOne = Double.parseDouble(this.numOne.getText());
        double numTwo = Double.parseDouble(this.numTwo.getText());
        double result;
        if(event.getSource()==addButton){
            result = numOne+numTwo;
        }else{
            result = numOne-numTwo;
        }
        result = roundTo2DecimalPlaces(result);
        this.result.setText(""+result);
    }


    /**
     * A library method that rounds a double to 2dp
     *
     * @param amount to round as a double
     * @return the amount rounded to 2dp
     */
    private double roundTo2DecimalPlaces(double amount) {
        return ((double) Math.round(amount * 100)) / 100;
    }

}