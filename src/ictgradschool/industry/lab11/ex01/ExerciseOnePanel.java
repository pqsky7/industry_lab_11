package ictgradschool.industry.lab11.ex01;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * A simple GUI app that does BMI calculations.
 */
public class ExerciseOnePanel extends JPanel implements ActionListener {

    // TODO Declare JTextFields and JButtons as instance variables here.
    private JButton calculateBMIButton;
    private JButton calculateHealthyWeightButton;
    private JTextField height_m;
    private JTextField weight_kg;
    private JTextField BMI;
    private JTextField maxHealthyWeight;

    /**
     * Creates a new ExerciseOnePanel.
     */
    public ExerciseOnePanel() {
        setBackground(Color.white);

        // TODO Construct JTextFields and JButtons.
        // HINT: Declare them as instance variables so that other methods in this class (e.g. actionPerformed) can
        // also access them.
        calculateBMIButton = new JButton("Calculate BMI");
        calculateHealthyWeightButton = new JButton("Calculate Healthy Weight");
        height_m = new JTextField(15);
        weight_kg = new JTextField(15);
        BMI = new JTextField(15);
        maxHealthyWeight = new JTextField(15);

        // TODO Declare and construct JLabels
        // Note: These ones don't need to be accessed anywhere else so it makes sense just to declare them here as
        // local variables, rather than instance variables.

        JLabel heightLabel = new JLabel("height in metres:");
        JLabel weightLabel = new JLabel("Weight in kilograms:");
        JLabel BMILabel = new JLabel("Your Body Mass Index (BMI) is:");
        JLabel maxHealthyWeightLabel = new JLabel("Maximum Healthy Weight for your Height:");

        // TODO Add JLabels, JTextFields and JButtons to window.
        // Note: The default layout manager, FlowLayout, will be fine (but feel free to experiment with others if you want!!)
        this.add(heightLabel);
        this.add(height_m);
        this.add(weightLabel);
        this.add(weight_kg);
        this.add(calculateBMIButton);
        this.add(BMILabel);
        this.add(BMI);
        this.add(calculateHealthyWeightButton);
        this.add(maxHealthyWeightLabel);
        this.add(maxHealthyWeight);

        // TODO Add Action Listeners for the JButtons
        calculateBMIButton.addActionListener(this);
        calculateHealthyWeightButton.addActionListener(this);

    }


    /**
     * When a button is clicked, this method should detect which button was clicked, and display either the BMI or the
     * maximum healthy weight, depending on which JButton was pressed.
     */
    public void actionPerformed(ActionEvent event) {

        // TODO Implement this method.
        // Hint #1: event.getSource() will return the button which was pressed.
        // Hint #2: JTextField's getText() method will get the value in the text box, as a String.
        // Hint #3: JTextField's setText() method will allow you to pass it a String, which will be diaplayed in the text box.
        double height = Double.parseDouble(height_m.getText());
        double weight = Double.parseDouble(weight_kg.getText());
        if(event.getSource()==calculateBMIButton){
            height = Double.parseDouble(height_m.getText());
            weight = Double.parseDouble(weight_kg.getText());
            double BMI = roundTo2DecimalPlaces(weight/(height*height));
            this.BMI.setText(""+BMI);
        }else{
            double maxHealthyWeight = roundTo2DecimalPlaces(24.9*height*height);
            this.maxHealthyWeight.setText(""+maxHealthyWeight);
        }
    }


    /**
     * A library method that rounds a double to 2dp
     *
     * @param amount to round as a double
     * @return the amount rounded to 2dp
     */
    private double roundTo2DecimalPlaces(double amount) {
        return ((double) Math.round(amount * 100)) / 100;
    }

}