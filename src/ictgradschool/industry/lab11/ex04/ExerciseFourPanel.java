package ictgradschool.industry.lab11.ex04;

import javax.swing.*;
import javax.swing.event.MouseInputListener;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.HashSet;

/**
 * Displays an animated balloon.
 */
public class ExerciseFourPanel extends JPanel implements ActionListener, KeyListener,MouseInputListener {

    private ArrayList<Balloon> balloon = new ArrayList<Balloon>();
    private JButton moveButton;
    private Timer timer;
    private int howMany;

    /**
     * Creates a new ExerciseFourPanel.
     */
    public ExerciseFourPanel() {
        setBackground(Color.white);
        balloon.add(new Balloon((int) (Math.random() * this.getSize().width), (int) (Math.random() * this.getSize().height)));



        //this.moveButton = new JButton("Move balloon");
        //this.moveButton.addActionListener(this);
        //this.add(moveButton);

        this.addKeyListener(this);
        this.addMouseListener(this);
        this.setFocusable(true);
        timer = new Timer(10, this);

    }

    /**
     * Moves the balloon and calls repaint() to tell Swing we need to redraw ourselves.
     *
     * @param e
     */
    @Override
    public void actionPerformed(ActionEvent e) {

        //balloon.move();

        // Sets focus to the panel itself, rather than the JButton. This way, the panel can continue to generate key
        // events even after we've clicked the button.
        if (e.getSource()==timer) {
            for (int i = 0; i < balloon.size(); i++) {
                balloon.get(i).move(this.getSize());
                requestFocusInWindow();
                repaint();
            }
        }
    }

    /**
     * Draws any balloons we have inside this panel.
     *
     * @param g
     */
    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        for (int i = 0; i < balloon.size(); i++) {
            balloon.get(i).draw(g);
        }
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_S) {
            timer.stop();
        } else {
            timer.start();
        }


        howMany = (int)(Math.random() * balloon.size() + 1);

        HashSet<Integer> indexes = new HashSet<>();

        for (int i = 0; i < howMany; i++) {
            int index;
            while (true) {
                 index = (int) (Math.random() * balloon.size());
                if(!indexes.contains(index)) {
                    indexes.add(index);
                    break;
                }
            }
            switch (e.getKeyCode()) {
                case KeyEvent.VK_UP:
                    balloon.get(index).setDirection(Direction.Up);
                    break;
                case KeyEvent.VK_DOWN:
                    balloon.get(index).setDirection(Direction.Down);
                    break;
                case KeyEvent.VK_LEFT:
                    balloon.get(index).setDirection(Direction.Left);
                    break;
                case KeyEvent.VK_RIGHT:
                    balloon.get(index).setDirection(Direction.Right);
                    break;
            }
        }



    }

    @Override
    public void keyReleased(KeyEvent e) {

    }

    @Override
    public void mouseClicked(MouseEvent e) {
        if(e.getButton()==MouseEvent.BUTTON1){
            balloon.add(new Balloon((int) (Math.random() * 801), (int) (Math.random() * 801)));
            requestFocusInWindow();
            repaint();
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }

    @Override
    public void mouseDragged(MouseEvent e) {

    }

    @Override
    public void mouseMoved(MouseEvent e) {

    }
}
